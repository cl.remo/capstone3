import Banner from '../components/Banner.js';
import FeaturedProducts from '../components/FeaturedProducts.js';

export default function Home() {
	return(
		<>
			<Banner />
			<FeaturedProducts />
		</>
	)
}