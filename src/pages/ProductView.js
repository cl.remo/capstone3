import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

      const { user } = useContext(UserContext)

      const { productId } = useParams();

      const navigate = useNavigate();
      //States
      const [name, setName] = useState("")
      const [description, setDescription] = useState("")
      const [price, setPrice] = useState(0);

      const order = (productId) => {
            fetch(`${process.env.REACT_APP_API_URL}/api/users/order`, {
                  method: "POST",
                  headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                  },
                  body: JSON.stringify({
                        productId: productId
                  })
            })
            .then(res => res.json())
            .then(data => {
                  console.log(data)

                  if(data === true) {
                        Swal.fire({
                            title: "Successfully ordered",
                            icon: "success",
                            text: "You have successfully ordered for this product."
                        })

                        navigate("/products");

                  } else {
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
                  }
            })
      };


      useEffect(() => {
            console.log(productId);

            fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                  console.log(data);

                  setName(data.name);
                  setDescription(data.description);
                  setPrice(data.price);

            })

      }, [productId]);

      return (

            <Container className="mt-5">
                  <Row>
                        <Col lg={{span: 6, offset: 3}}>
                              <Card>
                                    <Card.Body className="text-center">
                                          <Card.Title>{name}</Card.Title>

                                          <Card.Subtitle>Description:</Card.Subtitle>
                                          <Card.Text>{description}</Card.Text> 

                                          <Card.Subtitle>Price:</Card.Subtitle>
                                          <Card.Text>{price} Gold Coins</Card.Text>

{/*                                          <Card.Subtitle>Class Schedule:</Card.Subtitle>
                                          <Card.Text>8 AM - 5 PM</Card.Text>*/}

                                          {
                                                (user.id !== null) ?
                                                      <Button variant="primary" onClick={() => order(productId)} >Order</Button>
                                                :
                                                      <Link className="btn btn-danger" to="/login">Login to Buy!</Link>
                                          }
                                        
                                    </Card.Body>
                              </Card>
                        </Col>
                  </Row>
            </Container>

      )
}