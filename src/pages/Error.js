import { Container, Row, Col, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export default function Error() {
	return (
		<Container>
			<Row>
				<Col className="p-5 text-center">
					<h1>404 - Not Found</h1>
					<h5>The Page you are looking for cannot be found :( </h5>
					<Nav.Link as={NavLink} to="/">Back Home</Nav.Link>
				</Col>
			</Row>
		</Container>
	)
}