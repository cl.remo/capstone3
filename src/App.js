import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Error from './pages/Error.js';
import Login from './pages/Login.js';
import Profile from './pages/Profile.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import AddProduct from './pages/AddProduct.js';
import ProductView from './pages/ProductView.js'
import Products from './pages/Products.js';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id : null,
    isAdmin : null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // used to check if the user information is properly stored upon login and that the localStorage is cleared upon logout.
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  // to implement hot reload immediately
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin : data.isAdmin
        });
      } else {
        setUser({
          id : null,
          isAdmin : null
        })
      }
    })
  }, [])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container>
          <AppNavBar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/products" element={<Products />} />
            <Route path="/register" element={<Register />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
