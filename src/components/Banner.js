import { useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import UserContext from '../UserContext';

export default function Banner() {

	const { user } = useContext(UserContext);
	console.log(user);

	return (
		<Row>
			<Col className="p-5 text-center">
				{
					(user.isAdmin === true) ?
					<>
						<h1>Welcome Admin</h1>
						<h3>Manage thy wares</h3>
					</>
					:
					<>
						<h1>Welcome to the Hollyzog's</h1>
						<h3>Got the wares, if you've got the coins</h3>
					</>
				}
				{(user.id !== null) ?
						user.isAdmin ?
							<Link className="btn btn-success" to="/products">Manage My Wares</Link>
						:
							<Link className="btn btn-success" to="/products">Browse My Wares</Link>
					:
					<>
						<Link className="btn btn-primary" to="/login">Login</Link>
						<Link className="btn btn-success" to="/register">Register</Link>
					</>
				}
			</Col>
		</Row>
	)
}