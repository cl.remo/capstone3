import { useState, useEffect } from 'react';
import ProductSearch from './ProductSearch.js';
import ProductCard from './ProductCard.js';
// import SearchByPrice from './SearchByPrice.js';

export default function UserView({productsData}) {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		const productsArr = productsData.map(product => {
			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id} />
				)
			} else {
				return null;
			}
		})

		setProducts(productsArr);
	}, [productsData])


	return (

		<>
			<h1 className="text-center">Products</h1>
			<ProductSearch />
			{ products }
		</>
	)

}