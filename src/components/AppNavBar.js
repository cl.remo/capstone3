import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {

	const { user } = useContext(UserContext);
	console.log(user);

	return(
		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand as={Link} to="/" >Hollyzog's</Navbar.Brand>

				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse>
					<Nav className="ms-auto">
						<Nav.Link as={NavLink} to="/" >Home</Nav.Link>
						{(user.isAdmin === true) ?
							<Nav.Link as={NavLink} to="/products" >Admin Dashboard</Nav.Link>
							:
							<Nav.Link as={NavLink} to="/products" >Products</Nav.Link>
						}

						{(user.id !== null) ?
							user.isAdmin ?
								<>
									<Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>
								:
								<>
									<Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>
							:
							<>
								<Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}