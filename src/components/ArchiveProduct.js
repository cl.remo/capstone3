import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product, isActive, fetchData}) {

	const archiveProduct = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/archive`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title : 'Success!',
					icon : 'success',
					text : 'Product archived successfully.'
				});
				fetchData();
			} else {
				Swal.fire({
					title : 'Oh! Oh!',
					icon : 'error',
					text : 'Please try again.'
				});
				fetchData();
			}
		})
	}

	const activateProduct = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/activate`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title : 'Success!',
					icon : 'success',
					text : 'Product activated successfully.'
				});
				fetchData();
			} else {
				Swal.fire({
					title : 'Oh! Oh!',
					icon : 'error',
					text : 'Please try again.'
				});
				fetchData();
			}
		})
	}

	return(
		<>
			{isActive ?
				<Button 
					variant="danger" 
					size="sm"
					type="Submit"
					onClick={() => archiveProduct(product)}
				>
					Archive
				</Button>
			:
				<Button 
					variant="success" 
					size="sm"
					type="Submit" 
					onClick={() => activateProduct(product)}
				>
					Activate
				</Button>
			}	
		</>
	)
}