import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({productProp}){

	const {name, description, price, _id} = productProp;
	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text> 

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price} Gold Coins</Card.Text>

			    <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}

// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}